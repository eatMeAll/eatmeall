module EatMeAll {

    requires javax.ws.rs.api;
    requires java.annotation;
    exports com.WildBirds.EatMeAll.domain.module;
}